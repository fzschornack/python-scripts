from bs4 import BeautifulSoup
import requests
from prefect.storage import GitLab
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from datetime import datetime, date, timedelta
import re
import pdfplumber
import os
from os import listdir
from prefect import task, Flow, Parameter
from prefect.schedules import IntervalSchedule
from prefect.executors import LocalDaskExecutor
import subprocess
from dotenv import load_dotenv

INITIAL_DATE = date.today()
TARGET_FOLDER = './temp/'
NAME_LIST = ['zschorn','lohan','catunda']

load_dotenv()

GITLAB_ACCESS_TOKEN = os.getenv("GITLAB_ACCESS_TOKEN")

print(GITLAB_ACCESS_TOKEN)

def _gte_link_date(link_string, initial_date):
    """Extract the pdf date from the link string and compare it with an initial date"""

    try:
        string_date = re.search('\d\d/\d\d/\d\d\d\d', link_string)
        link_date = datetime.strptime(string_date.group(0), '%d/%m/%Y').date()
        return link_date >= initial_date
    except:
        return False


@task(max_retries=3, retry_delay=timedelta(seconds=15), log_stdout=True)
def download_pdfs(initial_date: date, target_folder: str) -> None:
    """Download all vaccination list pdfs that are greater than or equal a certain initial date
    and save them in a local folder"""

    url = 'https://coronavirus.fortaleza.ce.gov.br/lista-vacinacao-d1.html'
    os.makedirs(target_folder, exist_ok=True)

    try:
        options = webdriver.ChromeOptions()
        options.add_argument('headless')
        browser = webdriver.Chrome(
            ChromeDriverManager().install(), options=options)
        browser.get(url)
        html = browser.page_source
        soup = BeautifulSoup(html, 'html.parser')

        links = soup.find_all("a")
        links_pdfs_d2_last_days = [link for link in links if (
            link.string and "D2" in link.string and _gte_link_date(link.string, initial_date))]

        for a in links_pdfs_d2_last_days:
            link_pdf = a['href']
            try:
                pdf = requests.get(link_pdf)
                pdf_name = link_pdf.split("/")[-1]
                filepath = target_folder + pdf_name
                with open(filepath, 'wb') as f:
                    f.write(pdf.content)
                    print("Added: " + filepath)
            except Exception as e:
                print(e)

    except Exception as e:
        print(e)
        raise


@task(max_retries=3, retry_delay=timedelta(seconds=15), log_stdout=True)
def finding_names_in_pdfs(name_list: list, target_folder: str) -> None:
    """Find the occurrences of names in vaccination list pdfs"""

    try:
        files = listdir(target_folder)
        for filename in files:
            print(filename)
            with pdfplumber.open(target_folder + filename) as pdf:
                for page in pdf.pages:
                    text = page.extract_text().lower()
                    # print(text)
                    for name in name_list:
                        if name in text:
                            print(f"{name} found in {filename}")
                        if len(name_list) == 0:
                            break
    except Exception as e:
        print(e)
        raise


@task(max_retries=3, retry_delay=timedelta(seconds=15), log_stdout=True)
def finding_names_in_pdfs_jar(name_list: list, target_folder: str) -> None:
    """Find the occurrences of names in vaccination list pdfs"""

    try:
        files = listdir(target_folder)
        for filename in files:
            print(filename)
            result = subprocess.run(['java', '-cp', 'resources/pdf-text-search-assembly-0.1.jar', 'com.fzschornack.pdf_text_search.PDFTextSearch',
                                     target_folder + filename,
                                     ','.join(name_list)], stdout=subprocess.PIPE)\
                .stdout.decode('utf-8')
            print(result)
    except Exception as e:
        print(e)
        raise


@task(max_retries=3, retry_delay=timedelta(seconds=15), log_stdout=True)
def remove_old_pdfs(target_folder: str) -> None:
    try:
        files = listdir(target_folder)
        for filename in files:
            os.remove(target_folder + filename)
    except Exception as e:
        print(e)
        raise


if __name__ == '__main__':
    schedule = IntervalSchedule(
        start_date=datetime.utcnow() + timedelta(seconds=1),
        interval=timedelta(minutes=5),
    )

    with Flow("vaccination-etl", schedule=schedule, executor=LocalDaskExecutor()) as flow:
        flow.chain(
            download_pdfs(INITIAL_DATE, TARGET_FOLDER),
            finding_names_in_pdfs_jar(NAME_LIST, TARGET_FOLDER),
            remove_old_pdfs(TARGET_FOLDER)
        )


    flow.storage = GitLab(
        repo="org/repo",  # name of repo
        path="flows/my_flow.py",  # location of flow file in repo
        access_token_secret=GITLAB_ACCESS_TOKEN # name of personal access token secret
    )

    # flow.register(project_name="etls", labels=['windows-agent'])
    # finding_names_in_pdfs_jar.run(NAME_LIST, TARGET_FOLDER)
